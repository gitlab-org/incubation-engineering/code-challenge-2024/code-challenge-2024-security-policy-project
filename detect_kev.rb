#!/usr/bin/env ruby

require 'json'

# Download https://www.cisa.gov/sites/default/files/feeds/known_exploited_vulnerabilities.json
kev_catalog = JSON.parse(File.read('./known_exploited_vulnerabilities.json')) 
known_exploited_vulnerabilities = kev_catalog['vulnerabilities']

dependency_scanning_report = JSON.parse(File.read('./gl-dependency-scanning-report.json'))

kev_cves = known_exploited_vulnerabilities.map do |vuln|
  vuln['cveID']
end

detected_kev_vulnerabilities = dependency_scanning_report['vulnerabilities'].select do |vuln|
  vuln['identifiers'].any? do |id|
    kev_cves.include?(id['value'])
  end
end

report = {}
File.open('gl-known-exploited-vulnerabilities-report.json', 'w') do |f|
  f.puts JSON.pretty_generate(report)
end

if ENV['CI']
  warn 'Skipping debug session in CI'
else
  warn 'Opening debug session'
  require 'pry'
  binding.pry
end
